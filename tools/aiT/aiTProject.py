# -*- coding: utf-8 -*-

from pathlib import Path
import subprocess as sp

from aiT.Apx import ApxFile

class aiTProject(object):
	def __init__(self, name, path : Path):
		self.name = name
		self.path = path
		self.apx = ApxFile(name, path.absolute())
		self.ais = {}
		self.results = {} # Store the different result analysis per aiTAnalysis

		self.path.mkdir(parents=True, exist_ok=True)

	def addAnalysis(self, analysis):
		analysisName = analysis.name

		analysisPath = self.path.joinpath(analysisName).absolute()

		analysisPath.mkdir(parents=True, exist_ok=True)

		analysis.path = analysisPath

		self.apx.addAnalysis(analysis)

		self.results[analysisName] = dict()

	def genAis(self, name):
		newAis = AisFile(self.path, name)
		self.ais[name] = newAis

		return newAis

	def run(self):
		self.apx.genFile()

		for aisF in self.ais.values():
			aisF.write()

		p = sp.run(['alauncher','-b','-j','-1',str(self.apx.path.absolute())], stdout=sp.PIPE)

		if p.returncode != 0:
			raise Exception("AiT call failed : %s" % self.name)

	def __str__(self):
		return "< AiTProject %s >" % self.name

	def __repr__(self):
		return self.__str__()
