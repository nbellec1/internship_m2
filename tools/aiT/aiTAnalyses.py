# -*- coding: utf-8 -*-

from aiT.ais import AisFile
from pathlib import Path

class aiTAnalysis(object):
	def __init__(self, name):
		self.name = name
		self.start = None
		self.xmlReport = ""
		self.htmlReport = ""
		self.txtReport = ""
		self.gdlReport = ""
		self.path = Path(name)
		self.ais = None
		self.type  = None

	def write(self, indent = 0):
		if self.path == None or self.start == None or (self.xmlReport == "" and self.htmlReport == "" and self.txtReport == "" and self.gdlReport == "") or self.type == None:
			raise Exception("""Unable to write the following analysis because some data are missing""", self)

		if self.ais != None:
			self.ais.write()

		data = "  " * indent + """<analysis xmlns="http://www.absint.com/apx" type=\"%s\" enabled=\"true\" id=\"%s\">\n""" % (self.type, self.name)

		indent += 1
		data += "  " * indent + """<analysis_start xmlns="http://www.absint.com/apx">%s</analysis_start>\n""" % self.start

		if self.ais != None:
			data += "  " * indent + """<ais xmlns="http://www.absint.com/apx">%s</ais>\n""" % str(self.ais.path)
		if self.xmlReport != "":
			data += "  " * indent + """<xml_report xmlns="http://www.absint.com/apx">%s</xml_report>\n""" % self.path.joinpath(self.xmlReport).absolute()
		if self.htmlReport != "":
			data += "  " * indent + """<html_report xmlns="http://www.absint.com/apx">%s</html_report>\n""" % self.path.joinpath(self.htmlReport).absolute()
		if self.txtReport != "":
			data += "  " * indent + """<report xmlns="http://www.absint.com/apx">%s</report>\n""" % self.path.joinpath(self.txtReport).absolute()
		if self.gdlReport != "":
			data += "  " * indent + """<gdl xmlns="http://www.absint.com/apx">%s</gdl>\n""" % self.path.joinpath(self.gdlReport).absolute()

		indent -= 1
		data += "  " * indent + """</analysis>\n"""

		return data

	def genAis(self, name):
		self.ais = AisFile(self.path, name)
		return self.ais

	def getReportPath(self, report):
		if report == "xml" and self.xmlReport != "":
			return self.path.joinpath(self.xmlReport).absolute()

		elif report == "html"and self.htmlReport != "":
			return self.path.joinpath(self.htmlReport).absolute()

		elif report == "txt"and self.txtReport != "":
			return self.path.joinpath(self.txtReport).absolute()

		elif report == "gdl"and self.gdlReport != "":
			return self.path.joinpath(self.gdlReport).absolute()

		else:
			raise Exception("Unable to parse the ask report type")

class aiTCfgAnalysis(aiTAnalysis):
	def __init__(self, name):
		super().__init__(name)
		self.type = 'control_flow_graph'

	def write(self, indent = 0):
		return super().write(indent)

class aiTWcetAnalysis(aiTAnalysis):
	def __init__(self, name):
		super().__init__(name)
		self.type = 'wcet_analysis'

	def write(self, indent = 0):
		return super().write(indent)
