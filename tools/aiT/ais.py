# -*- coding: utf-8 -*-

from pathlib import Path
from utils import loggingConf

class AisLoop(object):
	def __init__(self, label="", lower=0, upper="inf"):
		self.label = label
		self.address = None
		self.lower = lower
		self.upper = upper

	def writeWithLabel(self, indent = 0):
		return "  "*indent + "loop \"%s\" { bound: %s .. %s; }\n" % (self.label, str(self.lower), str(self.upper))

	def writeWithAddress(self, indent = 0):
		return "  "*indent + "loop %s { bound: %s .. %s; }\n" % (hex(self.address), str(self.lower), str(self.upper))

	def write(self, indent = 0):
		if self.address != None:
			return self.writeWithAddress(indent)
		else:
			return self.writeWithLabel(indent)

class AisSnippetNotAnalyzed(object):
	def __init__(self, start, continueAt):
		self.start = start
		self.continueAt = continueAt

	def write(self, indent = 0):
		data = "  "*indent + "instruction %s snippet {\n" % str(self.start)

		indent += 1
		data += "  "*indent + "continue at: %s;\n" % str(self.continueAt)
		data += "  "*indent + "not analyzed;\n"
		data += "  "*indent + "takes: 0 cycles;\n"

		indent -= 1
		data += "  "*indent + "}\n"

		return data

class AisRecursion(object):
	def __init__(self, label="", lower=0, upper="inf"):
		self.label = label
		self.lower = lower
		self.upper = upper

	def write(self, indent = 0):
		return "  "*indent + "routine \"%s\" { recursion bound: %s .. %s; }\n" % (self.label, str(self.lower), str(self.upper))

class AisTryZone(object):
	def __init__(self):
		self.insideInst = list()

	def addInstruction(self, inst):
		self.insideInst.append(inst)

	def write(self, indent = 0):
		s = "  "*indent + "try {\n"
		for inst in self.insideInst:
			s += inst.write(indent+1)
		s += "  "*indent + "}\n"

		return s

class AisEvaluateZone(object):
	def __init__(self, start, end, name, inclusive=True):
		self.start = start
		self.end = end
		self.name = name
		self.inclusive = inclusive

	def write(self, indent = 0):
		if self.inclusive:
			return "  "*indent + "evaluate %s to %s inclusive as : \"%s\";\n" % (str(self.start), str(self.end), self.name)
		else:
			return "  "*indent + "evaluate %s to %s exclusive as : \"%s\";\n" % (str(self.start), str(self.end), self.name)

class AisLabel(object):
	def __init__(self, name, value):
		self.name = name
		self.value = value

	def write(self, indent = 0):
		return "  "*indent + "label \"%s\": %s;\n" % (self.name, self.value)

class AisDefine(object):
	def __init__(self, name, value):
		self.name = name
		self.value = value

	def write(self, indent = 0):
		return "  "*indent + "define \"%s\": %s;\n" % (self.name, self.value)

class AisInclude(object):
	def __init__(self, path : Path):
		self.path = path.absolute()

	def write(self, indent = 0):
		return "  "*indent + "include '%s';\n" % (str(self.path))

class AisUnknown(object):
	def __init__(self, unknown):
		self.unknown = unknown

	def write(self, indent = 0):
		return "  "*indent + self.unknown

class AisFile(object):
	logger = loggingConf.getLogger("AisFile")

	def __init__(self,  path : Path = Path('.'), name = ""):
		self.name = name + '.ais'
		self.path = path.joinpath(self.name).absolute()
		self.data = list()

	def addInstruction(self, inst):
		self.data.append(inst)

	def write(self):
		data = ""

		for d in self.data:
			data += d.write(indent = 0)

		if self.path.exists():
			self.logger.warning(""" The following ais file already exists and will be overwritten : %s """ % str(self.path))

		with open(str(self.path), 'w') as f:
			f.write(data)
