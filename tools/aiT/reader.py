# -*- coding: utf-8 -*-

from pathlib import Path
import re
import xml.etree.ElementTree as ET

from aiT.ais import *
from aiT.xmlAnalyses.aiTXml import aiTXml
from utils import loggingConf

class AisReader:
	logger = loggingConf.getLogger("AisReader")

	def __init__(self):
		""" Virtually private constructor """
		raise Exception("The class AisReader only contains static method")

	@staticmethod
	def analyzeAis(path : Path):
		aisPath = str(path.absolute())

		rawData = []
		aisData = AisFile(path.absolute())

		# Read the file and suppres the comments
		with open(aisPath, 'r') as f:
			for line in f:
				l = line.strip()

				if len(l) > 0 and l[0] != "#":
					rawData.append(l)
		"""
		For now, we only treat annotation of the type :
			loop <label> { bound: <int> .. <int>; }
			routine <label> { recursion bound: <int> .. <int>; }

		All other annotations are stripped
		Furthermore, we require that no space are introduce in label names and to respect the exact spacing of the previous annotations
		"""

		loopRegex = re.compile('loop "(?P<label>[^"]*)" { bound: (?P<lower>((0x)|0|b)?[0-9a-fA-F]*) .. (?P<upper>((0x)|0|b)?[0-9a-fA-F]*); }')
		recursionRegex = re.compile('routine "(?P<label>[^"]*)" { recursion bound: (?P<lower>((0x)|0|b)?[0-9a-fA-F]*) .. (?P<upper>((0x)|0|b)?[0-9a-fA-F]*); }')

		for line in rawData:
			m = loopRegex.match(line)
			if m != None:
				loop = AisLoop(m.group('label'), m.group('lower'), m.group('upper'))
				aisData.addInstruction(loop)
				continue

			m = recursionRegex.match(line)
			if m != None:
				rec = AisRecursion(m.group('label'), m.group('lower'), m.group('upper'))
				aisData.addInstruction(rec)
				continue

			AisReader.logger.warning("""Ignoring line : %s""" % str(line))

			aisData.addInstruction(AisUnknown(line))

		return aisData

class AiTXmlReader(object):
	logger = loggingConf.getLogger("AiTXmlReader")

	def __init__(self):
		""" Virtually private constructor """
		raise Exception("The class AiTXmlReader only contains static method")

	@staticmethod
	def readXml(path : Path):
		xmlPath = str(path.absolute())

		rawXml = ET.parse(xmlPath)

		return aiTXml(rawXml, xmlPath)
