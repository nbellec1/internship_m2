# -*- coding: utf-8 -*-

from collections import namedtuple

class aiTXml(object):

	ns = {'a3_report':'http://www.absint.com/a3report'}
	aiTBB = namedtuple('aiTBB', ['firstInstruction','lastInstruction','loopClass'])

	def __init__(self, rawXml, name=""):
		self.rawXml = rawXml
		self.name = name
		self.results = dict()

	def wcetRegionAnalysis(self):
		"""
		Analyse the WCET result of each regions specified to aiT
		"""
		self.results['wcetRegionAnalysis'] = {}

		root = self.rawXml.getroot()

		wcetRegions = root.findall('.//a3_report:wcet_analysis/a3_report:wcet_results/a3_report:wcet_region[@name]', aiTXml.ns)

		for region in wcetRegions:
			name = region.attrib['name']
			wcet = region.attrib['cumulative_cycles']

			self.results['wcetRegionAnalysis'][name] = int(wcet)

	def loopExtraction(self):
		"""
		Extract the loops and basic information on them
		"""
		analysisName = 'loopExtraction'
		if analysisName in self.results:
			self.logger.warn("The following analysis have already been performed but will be overwritten : %s " % analysisName)

		self.results[analysisName] = dict()

		root = self.rawXml.getroot()
		routines = root.findall('.//a3_report:cfg_value_analysis/a3_report:routines', aiTXml.ns)[0]

		for r in routines:
			atts = r.attrib
			if atts.get('loop'):
				# This is a loop, we retrieve the basic informations
				name = atts['name']
				extracted_from = atts['extracted_from']
				start = atts['address']

				self.results[analysisName][name] = dict()
				self.results[analysisName][name]['extracted_from'] = extracted_from
				self.results[analysisName][name]['start'] = start

	def loopAnalysis(self):
		analysisName = 'loopAnalysis'
		if analysisName in self.results:
			self.logger.warn("The following analysis have already been performed but will be overwritten : %s " % analysisName)

		self.results[analysisName] = {}

		root = self.rawXml.getroot()
		routines = root.findall('.//a3_report:cfg_value_analysis/a3_report:routines', aiTXml.ns)[0]

		# The first loop allows to save for each function the loops inside the function and for each routine the ids of the blocks

		for r in routines:
			atts = r.attrib
			if atts.get('loop'):
				# This is a loop, we retrieve the basic informations
				name = atts['name']
				extracted_from = atts['extracted_from']
				start = atts['address']

				self.results[analysisName].setdefault(extracted_from, {}).setdefault('loops', {}).setdefault(name, {})
				self.results[analysisName][extracted_from]['loops'][name]['start'] = start
				self.results[analysisName][extracted_from]['loops'][name]['block ids'] = set()
				self.results[analysisName][extracted_from]['loops'][name]['basic blocks'] = set()

				for block in r:
					bId = block.attrib['id']
					self.results[analysisName][extracted_from]['loops'][name]['block ids'].add(bId)

			else:
				name = atts['name']
				start = atts['address']

				self.results[analysisName].setdefault(name, {})['start'] = start
				self.results[analysisName][name]['block ids'] = set()

				for block in r:
					bId = block.attrib['id']
					self.results[analysisName][name]['block ids'].add(bId)

		for r in routines:
			atts = r.attrib

			if atts.get('loop') != None:
				self._analyzeOneLoop(r)

	def _analyzeOneLoop(self, loop):
		analysisName = 'loopAnalysis'

		name = loop.attrib['name']
		extracted_from = loop.attrib['extracted_from']
		start = loop.attrib['address']

		# Find all the "instruction blocks" i.e. the blocks that contains instruction adresses
		instructionBlocks = loop.findall('.//a3_report:first_instruction/..', aiTXml.ns)

		# Find the call blocks that can be interesting to find loop nesting
		callSuccessors = loop.findall("./a3_report:block[@type='call']/a3_report:successor", aiTXml.ns)

		for block in instructionBlocks:
			firstInstruction = block.find('./a3_report:first_instruction', aiTXml.ns).text
			lastInstruction = block.find('./a3_report:last_instruction', aiTXml.ns).text
			loopClass = block.find('./a3_report:loop_classification', aiTXml.ns).text

			bb = aiTXml.aiTBB(firstInstruction, lastInstruction, loopClass)

			self.results[analysisName][extracted_from]['loops'][name]['basic blocks'].add(bb)

		for successor in callSuccessors:
			bId = successor.text

			"""
			4 possibilities :
			* bId is a block of the loop itself -> nothing interesting
			* bId is in the function blocks -> we found an exit node, not that interesting
			* bId is in another function -> we found a real 'call' instruction that we probably already have ...
			* bId is in another loop of the same function -> we found a nested loop
			"""

			foundBlock = False

			# First possibility
			for blockId in self.results[analysisName][extracted_from]['loops'][name]['block ids']:
				if blockId == bId:
					foundBlock = True
					break

			if foundBlock:
				continue

			# Second possibility
			for blockId in self.results[analysisName][extracted_from]['block ids']:
				if blockId == bId:
					foundBlock = True
					break

			if foundBlock:
				continue

			# Fourth possibility (we won't check the 3rd possibility ^^ )
			for otherLoop in self.results[analysisName][extracted_from]['loops']:
				if foundBlock:
					break

				if otherLoop == name:
					continue

				for blockId in self.results[analysisName][extracted_from]['loops'][otherLoop]['block ids']:
					if blockId == bId:
						self.results[analysisName][extracted_from]['loops'][name].setdefault('innerLoop', []).append(otherLoop)
						self.results[analysisName][extracted_from]['loops'][otherLoop].setdefault('outerLoop', []).append(name)
						foundBlock = True
						break
