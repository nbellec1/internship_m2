# -*- coding: utf-8 -*-

from pathlib import Path

from utils import loggingConf

class ApxFile(object):

	logger = loggingConf.getLogger("ApxFile")

	def __init__(self, name, path : Path):
		self.name = name + ".apx"
		self.path = path.joinpath(self.name)
		self.analyses = {}
		self.executables = []

	def addAnalysis(self, analysis):
		name = analysis.name

		if name in self.analyses:
			self.logger.warn(""" The following analysis \"%s\" already exist and will be overwritten """ % name)

		self.analyses[name] = analysis

	def addExecutable(self, exe : Path):
		self.executables.append(exe.absolute())

	def genFile(self):
		data = self._genHeader(0)
		data += self._genExecutableFiles(1)
		data += self._genAnalysis(1)
		data += self._genOptions(1)
		data += self._genFooter(0)

		if self.path.exists():
			self.logger.warning(""" The following apx file already exists and will be overwritten : %s """ % str(self.path.absolute()))

		with open(str(self.path.absolute()), 'w') as f:
			f.write(data)

	def _genHeader(self, indent = 0):
		header = """<!DOCTYPE APX>\n"""
		header += """<project xmlns="http://www.absint.com/apx" build="3381670" xsi:noNamespaceSchemaLocation="http://www.absint.com/dtd/a3-apx-18.10.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" target="leon3" version="18.10">" build="3381670" xsi:noNamespaceSchemaLocation="http://www.absint.com/dtd/a3-apx-18.10.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" target="leon3" version="18.10">\n"""
		return header

	def _genExecutableFiles(self, indent = 0):
		data = "  " * indent + """<files xmlns="http://www.absint.com/apx">\n"""

		indent += 1
		data += "  " * indent + """<executables xmlns="http://www.absint.com/apx">"""
		for i in range(len(self.executables)):
			if i != 0:
				data += ','
			data += str(self.executables[i])

		data += """</executables>\n"""

		indent -= 1
		data += "  " * indent + """</files>\n"""

		return data

	def _genAnalysis(self, indent = 0):
		data = "  " * indent + """<analyses xmlns="http://www.absint.com/apx">\n"""

		indent += 1

		for analyze in self.analyses.values():
			data += analyze.write(indent)

		indent -= 1

		data += "  " * indent + """</analyses>\n"""
		return data

	def _genOptions(self, indent = 0):
		data = "  " * indent + """<options xmlns="http://www.absint.com/apx">\n"""
		indent += 1
		data += "  " * indent + """<analyses_options xmlns="http://www.absint.com/apx">\n"""
		indent += 1

		data += "  " * indent + """<report_file_verbosity_level xmlns="http://www.absint.com/apx">3</report_file_verbosity_level>\n"""
		data += "  " * indent + """<xml_report_file_verbosity_level xmlns="http://www.absint.com/apx">3</xml_report_file_verbosity_level>\n"""
		data += "  " * indent + """<pedantic_level xmlns="http://www.absint.com/apx">1</pedantic_level>\n"""
		data += "  " * indent + """<xml_call_graph xmlns="http://www.absint.com/apx">true</xml_call_graph>\n"""
		data += "  " * indent + """<xml_show_per_context_info xmlns="http://www.absint.com/apx">true</xml_show_per_context_info>\n"""
		data += "  " * indent + """<xml_wcet_path xmlns="http://www.absint.com/apx">true</xml_wcet_path>\n"""
		data += "  " * indent + """<xml_non_wcet_cycles xmlns="http://www.absint.com/apx">true</xml_non_wcet_cycles>\n"""
		data += "  " * indent + """<include_output_from_intermediate_decoding_rounds xmlns="http://www.absint.com/apx">true</include_output_from_intermediate_decoding_rounds>\n"""

		indent -= 1
		data += "  " * indent + """</analyses_options>\n"""
		data += "  " * indent + """<general_options xmlns="http://www.absint.com/apx">\n"""

		indent += 1
		data += "  " * indent + """<include_path xmlns="http://www.absint.com/apx">..</include_path>\n"""

		indent -= 1
		data += "  " * indent + """</general_options>\n"""
		data += "  " * indent + """<leon3_options>\n"""

		indent += 1
		data += "  " * indent + """<instruction_cache>\n"""

		indent += 1
		data += "  " * indent + """<enabled>false</enabled>\n"""

		indent -= 1
		data += "  " * indent + """</instruction_cache>\n"""
		data += "  " * indent + """<data_cache>\n"""

		indent += 1
		data += "  " * indent + """<enabled>false</enabled>\n"""

		indent -= 1
		data += "  " * indent + """</data_cache>\n"""
		data += "  " * indent + """<stack_pointer>0x80000000</stack_pointer>\n"""
		data += "  " * indent + """<stack_area>0x60000000 .. 0x80000000</stack_area>\n"""

		indent -= 1
		data += "  " * indent + """</leon3_options>\n"""

		indent -= 1
		data += "  " * indent + """</options>\n"""

		return data

	def _genFooter(self, indent = 0):
		return "  " * indent + """</project>"""
