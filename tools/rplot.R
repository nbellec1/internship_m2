#!/usr/bin/env Rscript

library(ggplot2)

args = commandArgs(trailingOnly=TRUE)

Pname <- args[1]

resFile = paste("localWcet",Pname,"partial.csv", sep="_") 
fullFile = paste("localWcet",Pname,"full.csv", sep="_")

res = read.csv(file=resFile, sep=";", header=TRUE)
full = read.csv(file=fullFile, sep=";", header=TRUE)
p = ggplot(res, aes(x=memory, y=log(currentGoal))) + geom_point() + 
	geom_line()
p <- p + geom_line(aes(y = log(full$currentGoal), color="red"))
p
