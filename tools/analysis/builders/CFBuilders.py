# -*- coding: utf-8 -*-

from typing import List
import re

from analysis.program import *
from analysis.CFG import *

from utils import loggingConf

class CFGBuilder(object):
	alwaysTrue = ['b', 'b,n', 'ba','ba,a','fba','fba,a','cba','cba,a','jmp']
	alwaysFalse = ['bn','bn,a','fbn','fbn,a','cbn','cbn,a']

	logger = loggingConf.getLogger("CFGBuilder")

	def __init__(self):
		""" Virtually private constructor """

		raise Exception("The class CFGBuilder is a static only class")

	@staticmethod
	def buildCFG(function: Function) -> CFG:
		CFGBuilder.logger.info("""Constructing CFG of %s""" % function.getName())

		cfg = CFG(function)

		BB = BasicBlock(start=function.getAddress(), end=function.getAddress()-4, function=function)
		delaySlotInstruction = False

		links = []
		targets = list()
		foundRet = False
		firstBB = True

		for instruction in function.getInstructions():
			BB.addInst()

			if delaySlotInstruction:
				# This is the last instruction so we add the resulting basic block to the function
				cfg.addBB(BB)
				CFGBuilder.logger.debug("""Added BB : %s """ % str(BB))

				for target in targets:
					links.append((BB.end, target))

				targets = list()

				if foundRet:
					cfg.linkToEnd(BB.end)

				if firstBB:
					cfg.linkToStart(BB.start)

				delaySlotInstruction = False
				foundRet = False
				firstBB = False
				CFGBuilder.logger.debug("""BB[%d] start : %s""" % (len(cfg.bbs.BBs), hex(BB.end + 4)))
				BB = BasicBlock(start=BB.end+4, end=BB.end, function=function)
				continue

			if instruction.getType() in ['branch', 'jump']:
				# A branch to treat

				delaySlotInstruction = True

				if type(instruction.getTarget()) == str and instruction.getTarget()[0] == '%':
					CFGBuilder.logger.warn("""Unable to determine the target for the following instruction :\n\t%s""" % instruction.getCode())
					function.safe = False

				else:
					target = int(instruction.getTarget(), base=16)
					if instruction.getOpcode() not in CFGBuilder.alwaysFalse:
						CFGBuilder.logger.debug("""Creating link based on the following opcode \"%s\"""" % str(instruction.getOpcode()))
						targets.append(target)

				if instruction.getOpcode() not in CFGBuilder.alwaysTrue:
					target = instruction.getAddress()+8
					CFGBuilder.logger.debug("""Creating link by fall-through to the following address %s""" % (hex(target)) )
					targets.append(target)

			elif instruction.getType() == 'ret':
				delaySlotInstruction = True
				foundRet = True

			elif instruction.getType() == 'call':
				delaySlotInstruction = True

				if type(instruction.getTarget()) == str and instruction.getTarget()[0] == '%':
					CFGBuilder.logger.warn("""Unable to determine the target for the following instruction :\n\t%s""" % instruction.getCode())
					function.safe = False
				else:
					try:
						callTarget = int(instruction.getTarget(), base=16)
					except Exception:
						function.safe = False
						raise Exception(instruction)

					BB.setCallTarget(callTarget)

				target = instruction.getAddress()+8
				CFGBuilder.logger.debug("""Creating link by fall-through to the following address %d""" % (target) )
				targets.append(target)

		if BB.start <= BB.end:
			# No CF instruction at the end of the function, so we add the last BB and treat it as unsafe
			CFGBuilder.logger.debug("""No control-flow instruction at the end of the last basic block""")
			cfg.addBB(BB)
			CFGBuilder.logger.debug("""Added BB : %s """ % str(BB))

			for target in targets:
				links.append((BB.end, target))

			cfg.linkToEnd(BB.end)
			function.safe = False

		CFGBuilder.logger.info("""Generating the graph""")
		CFGBuilder.logger.debug("""\tlinks : """)
		for (end, target) in links:
			CFGBuilder.logger.debug("""\t%s\t%s""" %(hex(end),hex(target)))

		for (end, target) in links:
			CFGBuilder.logger.debug("""Treating link : %s --> %s """ % (hex(end),hex(target)))
			try:
				cfg.addEdge(end, target)
			except CFException as e:
				CFGBuilder.logger.warn(str(e.args))
				CFGBuilder.logger.warn("""Maybe we hit the libc ?""")
				function.safe = False

		function.cfg = cfg
		return cfg

class ICFGBuilder:
	logger = loggingConf.getLogger("ICFGBuilder")

	def __init__(self):
		""" Virtually private constructor """
		raise Exception("The class ICFGBuilder is a static only class")

	@staticmethod
	def buildICFG(program : Program):
		ICFGBuilder.logger.info("""Constructing the ICFG of the program %s""" % program.getName())

		icfg = ICFG(program)
		program.icfg = icfg

		main = program.getFunctionFromName("main")
		stack = []

		ICFGBuilder._analyzeFunction(main, stack, icfg, program)

		if len(stack) != 0:
			ICFGBuilder.logger.warn(""" ICFG analysis of function finished with strange stack : %s""" % str(stack))

		return icfg

	@staticmethod
	def _analyzeFunction(function : Function, stack : List[Function], icfg : ICFG, program : Program):
		if function in stack:
			# We are in presence of a recursive function, we treat each function after its apparition as recursive as well
			indexF = stack.index(function)

			for i in range(indexF, len(stack)):
				stack[i].recursive = True
				ICFGBuilder.logger.debug("""Found the following recursive function : %s""" % stack[i].getName())

		if function in icfg:
			# The function has been/is already treated, return
			ICFGBuilder.logger.debug(""" Quick exit %s """ % function.getName())
			return

		ICFGBuilder.logger.debug("""Analyzing function %s""" % function.getName())

		stack.append(function)
		icfg.addFunction(function)

		for BB in function.cfg.bbs:
			if BB.callTarget != None:
				target = BB.callTarget
				callee = None
				index = program.funcAddMap.get(target)
				if index != None:
					callee = program.functions[index]

				if callee == None:
					ICFGBuilder.logger.warn(""" Unable to find a target function at address %s inside the function %s""" % (hex(target), f.getName()))
				else:
					ICFGBuilder._analyzeFunction(callee, stack, icfg, program)
					icfg.addCall(function, callee, BB)

		stack.pop()
