# -*- coding: utf-8 -*-

import networkx as nx
from collections import defaultdict
import pprint

from analysis.PDST import *
from analysis.program import *
from utils import loggingConf


class _CEClass:
	number = 0

	def __init__(self):
		self.first = True
		self.backedge = None
		self.count = 0
		self.number = _CEClass.number
		_CEClass.number += 1

	def isLast(self):
		return self.count == 1

	def inc(self, bracket=None):
		if bracket == None:
			self.count += 1

		else:
			if self.backedge != None and self.backedge != bracket:
				raise Exception("ASSERT backedge")
			if self.backedge == None:
				self.count += 1
			self.backedge = bracket

	def dec(self):
		self.count -= 1
		self.first = False

	def isFirst(self):
		return self.first

	def __repr__(self):
		return self.__str__()

	def __str__(self):
		return "< _CEClass "+str(self.number)+" >"

class PDSTBuilder(object):
	logger = loggingConf.getLogger("PDSTBuilder")

	def __init__(self):
		""" Virtually private constructor """
		raise Exception("The class PDSTBuilder is a static only class")

	@staticmethod
	def buildPDST(f : Function) -> PDST:
		PDSTBuilder.logger.info("""Constructing PDST of %s""" % f.getName())

		cfgGraph = f.cfg.getGraph()

		pdst = PDST(f)

		state = dict()
		state["dfsnum"] = dict()
		state["backedge"] = defaultdict(lambda: True)
		state["hi"] = defaultdict(lambda: -1)
		state["bset"] = defaultdict(list)
		state["capping"] = defaultdict(lambda: False)
		state["recentSize"] = defaultdict(lambda: 0)
		state["recentClass"] = dict()
		state["Class"] = defaultdict(lambda: None)

		state["nodes"] = [None]*len(cfgGraph)
		state["cur_dfsnum"] = 0

		cfgGraph.add_edge('end','start')

		PDSTBuilder.logger.debug("""Computing DFS """)

		PDSTBuilder._DFS('start', cfgGraph, state)

		PDSTBuilder.logger.debug("""DFS result : %s""" % str(state["nodes"]))

		PDSTBuilder.logger.debug("""Assigning classes to the edges """)

		PDSTBuilder._assignClasses(cfgGraph, state)

		cfgGraph.remove_edge('end','start')

		context = pdst.getRoot()

		PDSTBuilder.logger.debug("""Building Program Structure Tree """)

		PDSTBuilder._buildTree('start', context, cfgGraph, state, pdst, f)

		return pdst

	@staticmethod
	def _DFS(n, cfg_graph, state):
		bb = cfg_graph.nodes[n]
		cur_dfsnum = state["cur_dfsnum"]

		state["dfsnum"][n] = cur_dfsnum

		PDSTBuilder.logger.debug("""BB %s has DFSNUM %s""" %(str(n), str(cur_dfsnum)))

		state["nodes"][cur_dfsnum] = n
		state["cur_dfsnum"] +=1

		for t in cfg_graph.successors(n):
			if state["dfsnum"].get(t) == None:
				state["backedge"][(n,t)] = False
				PDSTBuilder._DFS(t, cfg_graph, state)

		for s in cfg_graph.predecessors(n):
			if state["dfsnum"].get(s) == None:
				state["backedge"][(s,n)] = False
				PDSTBuilder._DFS(s, cfg_graph, state)

	@staticmethod
	def _assignClasses(cfg_graph, state):
		for i in range(len(cfg_graph.nodes)-1, -1, -1):
			hi0 = len(cfg_graph.nodes)
			hi1 = hi0
			hi2 = hi0

			n = state["nodes"][i]

			PDSTBuilder.logger.debug("""Processing BB: %s (dfsnum == %d)""" % (n, state["dfsnum"][n]))

			# hi0 = min(t.dfsnum | (n,t) is a backedge)
			for t in cfg_graph.successors(n):
				dfsnum_t = state["dfsnum"][t]
				backedge = state["backedge"][(n,t)]
				PDSTBuilder.logger.debug("""Examining %s (dfsnum == %d)""" % (t, dfsnum_t))
				if  backedge and dfsnum_t < i and dfsnum_t < hi0:
					hi0 = dfsnum_t

			PDSTBuilder.logger.debug("""hi0 inter = %d""" % hi0)

			for t in cfg_graph.predecessors(n):
				dfsnum_t = state["dfsnum"][t]
				backedge = state["backedge"][(t,n)]
				PDSTBuilder.logger.debug("""Examining %s (dfsnum == %d)""" % (t, dfsnum_t))
				if  backedge and dfsnum_t < i and dfsnum_t < hi0:
					hi0 = dfsnum_t

			PDSTBuilder.logger.debug("""hi0 = %d""" % hi0)

			# hi1 = min( c.hi | c is a child of n )
			hichild = None

			for t in cfg_graph.successors(n):
				dfsnum_t = state["dfsnum"][t]
				hi = state["hi"][t]
				backedge = state["backedge"][(n,t)]
				PDSTBuilder.logger.debug("""Examining %s (dfsnum == %d)""" % (t, dfsnum_t))
				if not backedge and hi < i and hi < hi1 and dfsnum_t > i:
					hi1 = hi
					hichild = t

			PDSTBuilder.logger.debug("""hi1 inter = %d""" % hi1)

			for t in cfg_graph.predecessors(n):
				dfsnum_t = state["dfsnum"][t]
				hi = state["hi"][t]
				backedge = state["backedge"][(t,n)]
				PDSTBuilder.logger.debug("""Examining %s (dfsnum == %d)""" % (t, dfsnum_t))
				if not backedge and hi < i and hi < hi1 and dfsnum_t > i:
					hi1 = hi
					hichild = t

			PDSTBuilder.logger.debug("""hi1 = %d""" % hi1)
			PDSTBuilder.logger.debug("""hichild = %s""" % str(hichild))

			state["hi"][n] = min(hi1, hi0)
			PDSTBuilder.logger.debug("""hi = %d""" % state["hi"][n])

			if hichild:
				for t in cfg_graph.successors(n):
					dfsnum_t = state["dfsnum"][t]
					hi = state["hi"][t]
					backedge = state["backedge"][(n,t)]
					if hi < i and not backedge and hi < hi2 and dfsnum_t > i and t != hichild:
						hi2 = hi

				PDSTBuilder.logger.debug("""hi2 inter = %d""" % hi2)

				for t in cfg_graph.predecessors(n):
					dfsnum_t = state["dfsnum"][t]
					hi = state["hi"][t]
					backedge = state["backedge"][(t,n)]
					if hi < i and not backedge and hi < hi2 and dfsnum_t > i and t != hichild:
						hi2 = hi

			PDSTBuilder.logger.debug("""hi2 = %d""" % hi2)

			for t in cfg_graph.successors(n):
				dfsnum_t = state["dfsnum"][t]
				backedge = state["backedge"][(n,t)]
				if not backedge and dfsnum_t > i:
					PDSTBuilder.logger.debug("""Merging bracket list for child: %s""" % str(t))
					for b in state["bset"][t]:
						state["bset"][n].append(b)

			for t in cfg_graph.predecessors(n):
				dfsnum_t = state["dfsnum"][t]
				backedge = state["backedge"][(t,n)]
				if not backedge and dfsnum_t > i:
					PDSTBuilder.logger.debug("""Merging bracket list for child: %s""" % str(t))
					for b in state["bset"][t]:
						state["bset"][n].append(b)

			todel = []
			for b in state["bset"][n]:
				PDSTBuilder.logger.debug("""Considering edge : %s""" % str(b))
				s,t = b
				if (s == n and state["dfsnum"][t] > i) or (t == n and state["dfsnum"][s] > i):
					PDSTBuilder.logger.debug("""\tDeleting""")
					todel.append(b)

			for b in todel:
				s,t = b
				state["bset"][n].remove(b)
				if state["capping"][(s,t)]:
					cfg_graph.remove_edge(s,t)
					state["capping"][(s,t)] = False

			for t in cfg_graph.successors(n):
				dfsnum_t = state["dfsnum"][t]
				backedge = state["backedge"][(n,t)]
				if backedge and dfsnum_t < i:
					state["bset"][n].append((n,t))


			for t in cfg_graph.predecessors(n):
				dfsnum_t = state["dfsnum"][t]
				backedge = state["backedge"][(t,n)]
				if backedge and dfsnum_t < i:
					state["bset"][n].append((t,n))

			if hi2 < hi0:
				PDSTBuilder.logger.debug("""Adding capping edge from %s to %s""" % (n, state["nodes"][hi2]))
				cfg_graph.add_edge(n, state["nodes"][hi2])
				state["capping"][(n,state["nodes"][hi2])] = True
				state["bset"][n].append((n, state["nodes"][hi2]))

			if i != 0:
				edge = None
				for t in cfg_graph.successors(n):
					dfsnum_t = state["dfsnum"][t]
					backedge = state["backedge"][(n,t)]
					if not backedge and dfsnum_t < i:
						edge = (n,t)

				for t in cfg_graph.predecessors(n):
					dfsnum_t = state["dfsnum"][t]
					backedge = state["backedge"][(t,n)]
					if not backedge and dfsnum_t < i:
						edge = (t,n)

				if edge == None:
					raise Exception("ASSERT edge")

				topBracket = state["bset"][n][-1]
				currentSize = len(state["bset"][n])

				if state["recentSize"][topBracket] != currentSize:
					state["recentSize"][topBracket] = currentSize
					state["recentClass"][topBracket] = _CEClass()

				state["Class"][edge] = state["recentClass"][topBracket]
				PDSTBuilder.logger.debug("""Put edge %s in class %s""" % (str(edge), str(state["Class"][edge])))
				state["Class"][edge].inc()

				if state["recentSize"][topBracket] != len(state["bset"][n]):
					raise Exception("ASSERT recentSize != len(bset)")

				if state["recentSize"][topBracket] == 1:
					state["Class"][topBracket] = state["Class"][edge]
					PDSTBuilder.logger.debug("""Put edge (topBracket) %s in class %s""" % (str(edge), str(state["Class"][edge])))
					if topBracket != ('end','start'):
						state["Class"][topBracket].inc(topBracket)

	@staticmethod
	def _generateNewSESEContext(context, pdst_graph):
		index = 0

		while (context,index) in pdst_graph.nodes():
			index += 1

		return (context, index)

	@staticmethod
	def _buildTree(node, context, cfg_graph, state, pdst, function):
		pdst_graph = pdst.getGraph()

		for t in cfg_graph.successors(node):
			PDSTBuilder.logger.debug("""Examining edge %s --> %s""" % (str(node), str(t)))
			cl = state["Class"][(node,t)]

			if state["capping"][(node,t)]:
				raise Exception("ASSERT Capping")

			newContext = context

			if cl != None and (not cl.isFirst() or not cl.isLast()):
				if cl.isFirst():
					PDSTBuilder.logger.debug("""[INFO] Entering region w/ class %s and count = %s and context = %s""" % (str(cl), str(cl.count), str(context)))

					newContext = SESERegion((cl.number,0), (node,t), function)

					pdst_graph.add_node(newContext)
					pdst_graph.add_edge(context, newContext)

				elif not cl.isLast():
					PDSTBuilder.logger.debug("""Moving to adjacent region w/ context %s""" % str(context))
					context.exit = (node, t)

					previousContext = list(pdst_graph.predecessors(context))[0]

					if previousContext.isDomain():
						domain = previousContext
					else:
						domain = SESEDomain(cl.number, function)

						for pred in set(pdst_graph.predecessors(context)):
							pdst_graph.remove_edge(pred, context)
							pdst_graph.add_edge(pred, domain)

						pdst_graph.add_edge(domain, context)
						domain.seses.append(context)

					numcl, numse = context.num
					newContext = SESERegion((cl.number,numse+1), (node,t), function)

					pdst_graph.add_node(newContext)
					pdst_graph.add_edge(domain, newContext)
					domain.seses.append(newContext)

				else:
					PDSTBuilder.logger.debug("""Exiting region w/ context = %s""" % str(context))
					context.exit = (node, t)
					domain = list(pdst_graph.predecessors(context))[0]

					if domain.isDomain():
						parent = list(pdst_graph.predecessors(domain))[0]
					else:
						parent = domain

					newContext = parent

				cl.dec()

			if state["dfsnum"].get(t) != None:
				state["dfsnum"][t] = None
				PDSTBuilder._buildTree(t, newContext, cfg_graph, state, pdst, function)

			""" TODO : link directly to BB and not BB num """
			newContext.nodes.add(t)
