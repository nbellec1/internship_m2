# -*- coding: utf-8 -*-

from pathlib import Path

from utils.loggingConf import getLogger

class Watcher(object):
	def __init__(self, path : Path, name : str):
		self.memory = 0
		self.potentialGain = None
		self.realGain = None
		self.goal = 0
		self.type = None
		self.seseIds = None
		self.filePath = path.joinpath(name+'.csv').absolute()

		with open(str(self.filePath), 'a') as f:
			""" Put data into the file """
			f.write("%s;%s;%s;%s;%s;%s\n" % ("memory", "seseIds", "potentialGain", "realGain", "currentGoal", "type"))

	def write(self):
		""" Put the data into the csv and reset the watcher data and add 1 to the round counter """

		with open(str(self.filePath), 'a') as f:
			""" Put data into the file """
			f.write("%d;%s;%d;%d;%d;%s\n" % (self.memory, self.seseIds, self.potentialGain, self.realGain, self.goal, self.type))

	def setPotentialGain(self, potentialGoal):
		self.potentialGain = self.goal - potentialGoal

	def setRealGain(self, gain):
		self.realGain = gain

	def setGoal(self, goal):
		self.goal = goal

	def setType(self, type : str):
		self.type = type

	def setSeseIds(self, seseIds):
		self.seseIds = str(seseIds)

	def setMemory(self, memory):
		self.memory = memory
