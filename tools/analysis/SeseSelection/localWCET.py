# -*- coding: utf-8 -*-

from functools import reduce
from collections import namedtuple

from analysis.SeseSelection.Core import *
from utils import loggingConf

class SimpleMemoryModel(MemoryModel):
	def __init__(self, project):
		super().__init__(project)

	def memoryCost(self, selected):
		try:
			return len(selected)
		except:
			return reduce(lambda x,y : x + 1, selected, 0)

class localWcetSeseSelector(SeseSelector):

	potentialSelected = namedtuple('potentialSelected', 'sese potentialGoal')
	logger = loggingConf.getLogger("localWcetSeseSelector")

	def __init__(self, project, memory : MemoryModel):
		super().__init__(project, memory)

	def _computePotentialGoal(self, sese, subSese):
		gain = max(subSese.WCET_OOC, sese.WCET_OOC - sese.subSeseWCET[subSese])
		if self.memory.memoryCost([subSese]) > self.memory.getRemainingMemory():
			""" If there is no remaining memory """
			return -1

		return gain

	def select(self, solver, sese):
		function = sese._function
		P = function._program

		potentials = filter(lambda s : s.potentialGoal > 0,
			map(lambda subSese : self.potentialSelected(sese=subSese, potentialGoal=self._computePotentialGoal(sese, subSese)),
			sese.subSeseWCET.keys())
		)

		self.logger.debug("%s" % str({s:self._computePotentialGoal(sese, s) for s in sese.subSeseWCET.keys()}))

		selectedSese = min(potentials,
								key=lambda potent : potent.potentialGoal,
								default = None)

		if selectedSese == None:
			return list()

		self.watcher.setPotentialGain(sese.WCET_OOC - sese.subSeseWCET[selectedSese.sese])

		return [selectedSese.sese]
