# -*- coding: utf-8 -*-

"""
This file contains the core classes to perform the SESE selection :
* An interface for the different possible analyses
* A generic sese selector class that can integrate an analysis
* A generic sese selection solver that use the previous class to return a result

NB : All those classes modify informations in the different part of the program and project structure while trying to not let informations that are not needed anymore
"""

from collections import namedtuple
from functools import reduce
from pathlib import Path

from analysis.program import *
from analysis.CFG import *
from analysis.PDST import *
from analysis.predicats import *
from aiT.ais import *
from aiT.aiTAnalyses import aiTWcetAnalysis
from aiT.reader import *
from analysis.SeseSelection.Watcher import Watcher

import networkx as nx

SeseAisMetadata = namedtuple('SeseAisMetadata', 'startAdd endAdd id stopDecodingAdd hitTheEnd')

class MemoryModel(object):
	"""
	Model the memory relation between sese regions and the cost of selecting them
	"""

	def __init__(self, project):
		self.project = project
		self.remainingMemory = self.project.args.memory
		self.totalMemory = self.project.args.memory
		self.maximalMemoryRequired = None

	def memoryCost(self, selected) -> int:
		"""
		Compute the cost (in terms of memory) of selecting these sese regions
		"""
		return -1

	def getRemainingMemory(self) -> int :
		"""
		Compute the remaining memory
		"""
		return self.remainingMemory

	def update(self, selected):
		cost = self.memoryCost(selected)

		if self.remainingMemory < cost:
			raise Exception("Not enough memory to select these SESE")

		self.remainingMemory -= cost

	def computeMaximalMemoryRequired(self):
		"""
		Compute the maximal memory required to select all the regions and have the best possible result
		"""

		if self.maximalMemoryRequired != None:
			return

		selectedRegions = []

		for P in self.project.programs:
			for f in filter(isSecured, P.icfg):
				for node in filter(lambda s : s.isRegion() , f.getPdst()):
					selectedRegions.append(node)

		maximalMemoryCost = self.memoryCost(selectedRegions)
		self.maximalMemoryRequired = maximalMemoryCost

	def checkTakeAllSese(self):
		self.computeMaximalMemoryRequired()

		if self.maximalMemoryRequired <= self.totalMemory:
			return True

		else:
			return False


class SeseSelector(object):
	"""
	Implement a strategie to select Sese regions in order to solve the Sese selection problem
	"""

	def __init__(self, project, memoryModel: MemoryModel):
		self.project = project
		self.memory = memoryModel
		self.watcher = None

	def preHook(self, solver):
		"""
		Compute all the data the selector needs before being able to start selecting regions
		"""
		None

	def update(self, selected):
		"""
		Update to the selection
		"""
		None

	def select(self, *args, **kargs):
		"""
		Generate a list of sese regions or domain that will be selected
		"""
		None

class SeseSelectionSolver(object):
	"""
	Solve the problem of Sese Selection over a whole project
	"""

	def __init__(self, project, memoryModel : MemoryModel, selector : SeseSelector, watcher : Watcher = None):
		self.selector = selector
		self.project = project
		self.memory = memoryModel
		self.selected = list()
		self.goal = None
		self.IPDST = dict()
		self.updateCounter = 0
		self.watcher = watcher
		selector.watcher = watcher

		""" Required to be able to split domain """
		""" TODO : Deal with domain split """
		#self.generateIPDST()

	def recoverSeseAisMetadata(self, sese):
		""" Analyse the sese region and the cfg to decide where to place the end point and other sese feature for aiT """
		""" Return : startAdd, endAdd, stopDecodingAdd, hitTheEnd """

		function = sese._function
		P = function._program

		startAdd = None
		endAdd = None
		hitTheEnd = None
		stopDecodingAdd = None

		""" Differentiate between domain and region for the id and the entry and exit edges """
		if sese.isDomain():
			id = "Domain_%s_%d" % (P.name, sese.cls)

			if sese.seses[0].num[1] != 0:
				raise Exception("Failed to recover the entry sese region", sese)

			entryEdge = sese.seses[0].entry
			exitEdge = sese.seses[-1].exit

		elif sese.isRegion():
			id = "Region_%s_%d_%d" % (P.name, sese.num[0], sese.num[1])

			entryEdge = sese.entry
			exitEdge = sese.exit

		else:
			return

		if entryEdge[0] == 'start':
			""" We are at the beginning of a function, we must let 1 instruction free of sese for aiT"""
			startBBNum = entryEdge[1]

			""" Note : We considere that the first BB of a program is never 2 or less instructions """
			startAdd = function.cfg.bbs.BBs[startBBNum].start+0x4

		else:
			startBBNum = entryEdge[1]
			startAdd = function.cfg.bbs.BBs[startBBNum].start

		if exitEdge[1] == 'end':
			""" We are at the end of a function, we must let the return instruction (+ delay slot) free for aiT """
			endBBNum = exitEdge[0]

			""" Note : We considere that the last BB contains at least 3 instructions (1 inst + return + delay slot) """
			endAdd = function.cfg.bbs.BBs[endBBNum].end - 0x8
			hitTheEnd = True

		else:
			endBBNum = exitEdge[1]
			endBB = function.cfg.bbs.BBs[endBBNum]
			endAdd = endBB.start

			""" /!\ the > is very important because the delay slot is taken into account in the BB even if it is only a branch + delay slot"""
			if endBB.end - endBB.start == 4:
				# Only one instruction + delay slot in the BB, aiT does not appreciate so we put the end 1 BB later
				# We suppose the instruction will always be a bn or ba instruction and thus there is only 1 neighbors node
				nextBbNum = list(function.cfg.getGraph().neighbors(endBBNum))[0]
				nextBb = function.cfg.bbs.BBs[nextBbNum]
				stopDecodingAdd = nextBb.start
			else:
				stopDecodingAdd = endAdd+0x4

		sese.metadata = SeseAisMetadata(startAdd=startAdd, endAdd=endAdd, id=id, stopDecodingAdd=stopDecodingAdd, hitTheEnd=hitTheEnd)

	def generateIPDST(self):
		def generateSubIPDST(function, parent):
			P = function._program
			pdst = function.pdst
			icfg = P.icfg

			if pdst.getRoot() in self.IPDST[P]:
				""" Already passed by this function """
				return

			self.IPDST[P].update(pdst.getGraph().copy())

			if parent != None:
				self.IPDST[P].add_edge(parent, pdst.getRoot())

			""" Select PDST node that can contain basic block """
			for sese in filter(
					lambda s : sese.isRoot() or sese.isRegion(),
					pdst):

				seseNodes = map(lambda n : function.cfg.bbs.BBs[n], sese.nodes)
				for callee in icfg.getGraph().successors(function):
					bb = icfg.getGraph().edges[function, callee]['callBB']
					if bb in seseNodes:
						""" We found a call to another routine """
						generateSubIPDST(P, callee, sese)

		for P in self.project.programs:
			self.IPDST[P] = nx.DiGraph(name=P.name)

			main = P.getFunctionFromName("main")
			generateSubIPDST(main, None)

	def preHook(self):
		"""
		Compute pre-analysis required to perform the analysis
		"""
		self.seseMapping = dict()
		self.aisSeseHeaders = dict()

		""" Provide metadata for each sese """
		for P in self.project.programs:
			self.seseMapping[P] = dict()

			for function in filter(isSecured, P.icfg):
				for sese in filter(lambda x : not x.isRoot(), function.getPdst()):
					self.recoverSeseAisMetadata(sese)
					self.seseMapping[P][sese.metadata.id] = sese

		""" Finished providing metadata for each sese """

		""" Generate a generic AIS header used by all analysis on a program
				This AIS header contains :
					* The starting and ending address of each sese
					* The evaluate instruction for each sese
					* The include to the independante header of each program
		"""

		for P in self.project.programs:
			self.aisSeseHeaders[P] = AisFile(Path('.'), "seseHeader_%s" % P.name)

			""" Generate a try block in which we will place all the annotations """
			seseHeaderTryZone = AisTryZone()
			self.aisSeseHeaders[P].addInstruction(seseHeaderTryZone)

			for function in filter(isSecured, P.icfg):
				for sese in filter(lambda x : not x.isRoot(), function.getPdst()):
					metadata = sese.metadata

					""" We add the informations about the sese """
					seseHeaderTryZone.addInstruction(AisLabel("start_%s" % metadata.id, hex(metadata.startAdd)))
					seseHeaderTryZone.addInstruction(AisLabel("end_%s" % metadata.id, hex(metadata.endAdd)))
					seseHeaderTryZone.addInstruction(AisEvaluateZone('"start_%s"' % metadata.id, '"end_%s"' % metadata.id, metadata.id))

			""" We also include the independante header of the program """
			self.aisSeseHeaders[P].addInstruction(AisInclude(P.labelIndependantHeader.path.absolute()))
			self.aisSeseHeaders[P].write()

		""" Finished generating the generic AIS header """

		""" Compute the WCET_OOC of each sese """
		self.computeAllSese(prefix="preHook_")

		""" Store the sub sese for each sese """
		for P in self.project.programs:
			for function in filter(isSecured, P.icfg):
				for sese in filter(lambda x : not x.isRoot(), function.getPdst()):
					sese.subSeses = [ subSese for subSese in sese.subSeseWCET ]

		""" Now applies the preHook of the selector """
		self.selector.preHook(self)

	def getMaximalSelected(self):
		""" Return the selected region which represent the bottleneck of the selection """
		return 	max(self.selected, key=lambda selected : selected.WCET_OOC)

	def computeAllSese(self, prefix=""):
		"""
		Run aiT to compute the time of all seses
		"""

		"""
			Generate a new project for each program that will compute the WCET of each sese
			as well as the time taken by each sub-sese
		"""
		for P in self.project.programs:
			""" Prepare the project """
			seseHeaderPath = self.aisSeseHeaders[P].path

			aiTproj = self.project.genNewAiTProject(prefix + "%s" % P.name)
			aiTproj.apx.addExecutable(P.binaryPath)

			for function in filter(isSecured, P.icfg):
				for sese in filter(lambda x : not x.isRoot(), function.getPdst()):
					metadata = sese.metadata

					""" Analysis of the WCET OOC of the sese """
					analysisName = metadata.id
					seseAnalysis = aiTWcetAnalysis(analysisName)
					aiTproj.addAnalysis(seseAnalysis)

					seseAnalysis.start = "start"
					seseAnalysis.xmlReport = "%s.xml" % analysisName

					seseAnalysis.genAis(analysisName)

					seseAnalysis.ais.addInstruction(AisInclude(seseHeaderPath))
					seseAnalysis.ais.addInstruction(AisLabel("start", hex(metadata.startAdd)))

					if not metadata.hitTheEnd:
						seseAnalysis.ais.addInstruction(AisUnknown("end: %s;\n" % hex(metadata.stopDecodingAdd)))

					for subSese in sese.subSeses:
						if subSese in self.selected:
							seseAnalysis.ais.addInstruction(AisSnippetNotAnalyzed(hex(subSese.metadata.startAdd), hex(subSese.metadata.endAdd)))



			""" Execute the project by aiT """
			aiTproj.run()
			
			__import__('ipdb').set_trace(context=3)

			""" Recover the results and place all the data in appropriate structures """
			for id,analysis in aiTproj.apx.analyses.items():
				xmlResult = AiTXmlReader.readXml(analysis.getReportPath('xml'))
				aiTproj.results[id]['xml'] = xmlResult
				xmlResult.wcetRegionAnalysis()

				""" Find the corresponding function """
				sese = self.seseMapping[P][id]

				sese.WCET_OOC = xmlResult.results['wcetRegionAnalysis'][id]
				sese.subSeseWCET = dict()

				for subSeseId in xmlResult.results['wcetRegionAnalysis']:
					""" subSeseId is the sese, we ignore it """
					if subSeseId == id:
						continue

					subSese = self.seseMapping[P][subSeseId]

					sese.subSeseWCET[subSese] = xmlResult.results['wcetRegionAnalysis'][subSeseId]

	def update(self, newSelected):
		"""
		Update the goal with the new information provided after the selection of some regions
		and recompute all the datas
		"""

		self.updateCounter += 1

		self.selector.update(newSelected)
		self.memory.update(newSelected)

		for s in newSelected:
			self.selected.append(s)

		self.computeAllSese(prefix="update_%d_" % self.updateCounter)

		""" Update the goal of the analysis """
		self.goal = max(map(lambda s : s.WCET_OOC, self.selected))

	def solve(self):
		"""
		Call the associated seseSelector to select seseRegion until there is no memory left
		Each time new sese regions are selected, aiT is called to adapt the cost of the node and recompute the current maximal
		"""
		self.preHook()
		watcherType = "type"

		""" If the memory is large enough to store all sese regions """
		if self.memory.checkTakeAllSese():
			newSelectedSeses = list()
			for P in self.project.programs:
				for f in filter(isSecured, P.icfg):
					for sese in filter(lambda S : S.isRegion(), f.getPdst()):
						newSelectedSeses.append(sese)
			self.update(newSelectedSeses)
			if self.watcher != None:
				self.watcher.setMemory(self.memory.totalMemory)
				self.watcher.setGoal(self.goal)
				self.watcher.setPotentialGain(0)
				self.watcher.setRealGain(self.goal)
				self.watcher.setType(watcherType)
				self.watcher.setSeseIds(newSelectedSeses)
				self.watcher.write()

			return

		""" Else select the first regions of each program and update """
		firstSelected = list()
		for P in self.project.programs:
			main = P.getFunctionFromName("main")
			pdst = main.getPdst()
			for sese in pdst.getGraph().successors(pdst.getRoot()):
				firstSelected.append(sese)

		self.update(firstSelected)
		if self.watcher != None:
			self.watcher.setGoal(self.goal)
			self.watcher.setPotentialGain(self.goal)
			self.watcher.setRealGain(0)
			self.watcher.setSeseIds(firstSelected)
			self.watcher.setMemory(self.memory.totalMemory - self.memory.getRemainingMemory())
			self.watcher.setType(watcherType)
			self.watcher.write()

		while self.memory.getRemainingMemory() > 0:
			""" Recover the sese region that is the current bottleneck region """
			maximalSese = self.getMaximalSelected()
			newSelectedSeses = self.selector.select(self, maximalSese)

			if self.watcher != None:
				previousWcet = maximalSese.WCET_OOC

			if len(newSelectedSeses) == 0:
				""" Probably not enough place anymore for selecting sese"""
				break

			self.update(newSelectedSeses)

			if self.watcher != None:
				self.watcher.setGoal(self.goal)
				self.watcher.setSeseIds(newSelectedSeses)
				self.watcher.setMemory(self.memory.totalMemory - self.memory.getRemainingMemory())
				self.watcher.setType(watcherType)
				self.watcher.setRealGain(previousWcet - maximalSese.WCET_OOC)
				self.watcher.write()

		return
