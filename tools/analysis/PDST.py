# -*- coding: utf-8 -*-

import networkx as nx
import graphviz as gvz

from utils.graphUtility import GraphClass

class SesePart(object):
	"""
	Base class for constructing Sese regions and Sese domains
	"""

	def __init__(self, function):
		self.subSeseWCET = dict()
		self.subSeses = list()
		self._function = function

	def isRoot(self):
		return False

	def isDomain(self):
		return False

	def isRegion(self):
		return False

class SESERoot(SesePart):
	"""
	Root node of the PDST
	"""

	def __init__(self, function):
		super().__init__(function)
		self.nodes = set()

	def isRoot(self):
		return True

	def __str__(self):
		return "SESERoot"

	def __repr__(self):
		return self.__str__()

class SESERegion(SesePart):
	"""
	A canonical SESE region
	"""

	def __init__(self, num, entry, function):
		super().__init__(function)

		self.num = num
		self.entry = entry
		self.exit = None
		self.nodes = set()
		""" TODO  : Replace self.nodes by self.datas['nodes']"""

	def isRegion(self):
		return True

	def __str__(self):
		#return "< SESERegion "+str(self.num)+", entry: "+str(self.entry)+", exit: "+str(self.exit)+" >"
		return "SESERegion "+str(self.num)

	def __repr__(self):
		return self.__str__()

class SESEDomain(SesePart):
	"""
	A canonical SESE domain (i.e. the maximal sized SESE domains)
	"""

	def __init__(self, cls, function):
		super().__init__(function)

		self.seses = list()
		self.cls = cls

	def isDomain(self):
		return True

	def __str__(self):
		#return "< SESEDomain "+str(self.cls)+" >"
		return "SESEDomain "+str(self.cls)

	def __repr__(self):
		return self.__str__()

class PDST(GraphClass):
	"""
	The Program Domain Structure Tree of a function (containing SESE regions and SESE Domain)
	"""

	def __init__(self, function):
		super().__init__("DiGraph")

		self._function = function

		self.root = SESERoot(function)
		self.graph.add_node(self.root)

	def getRoot(self):
		return self.root
