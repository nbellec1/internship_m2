# -*- coding: utf-8 -*-

import networkx as nx
import graphviz as gvz

from analysis.program import BasicBlock, Function, Program
from utils import loggingConf
from utils.graphUtility import GraphClass

class CFException(Exception):
	def __init__(self, message):
		super().__init__(str(message))

class BasicBlockList(object):
	"""
	A list of basic block with fast retrieving for start and end address
	"""

	logger = loggingConf.getLogger("BBList")

	def __init__(self):
		self.BBs = list()
		self.startMap = dict()
		self.endMap = dict()

	def getBBFromStart(self, startAdd):
		index = self.getBBNumFromStart(startAdd)
		return self.BBs[index]

	def getBBFromEnd(self, endAdd):
		index = self.getBBNumFromEnd(endAdd)
		return self.BBs[index]

	def getBBNumFromStart(self, startAdd):
		index = self.startMap.get(startAdd)
		if index == None or len(self.BBs) <= index:
			raise CFException("There is no Basic Block starting at the following address : %s" % hex(startAdd))

		return index

	def getBBNumFromEnd(self, endAdd):
		index = self.endMap.get(endAdd)
		if index == None or len(self.BBs) <= index:
			raise CFException("There is no Basic Block ending at the following address : %s" % hex(endAdd))

		return index

	def addBB(self, bb):
		index = len(self.BBs)

		self.startMap[bb.start] = index
		self.endMap[bb.end] = index

		self.BBs.append(bb)

	def getBBNum(self, add):
		for i in range(len(self.BBs)):
			if add in self.BBs[i]:
				return i

		return -1

	def split(self, splitNum, splitAdd, splitAddToNewBB=True):
		"""
		Split a basic block designated by a 'splitNum' at an address 'splitAdd'
		"""
		if len(self.BBs) <= splitNum:
			raise CFException("The Basic Block to split does not exist")
		if splitAdd not in self.BBs[splitNum]:
			raise CFException("The Basic Block to split does not contain the splitting address")

		if splitAddToNewBB:
			return self._splitIntern(splitNum, splitAdd)
		else:
			return self._splitIntern(splitNum, splitAdd+4) # The factor is due to the constant size of Leon3 instructions

	def _splitIntern(self, splitNum, splitAdd):
		self.logger.debug(""" Splitting a BasicBlock at the address : %s """ % hex(splitAdd))

		previousBB = self.BBs[splitNum]

		if splitAdd == previousBB.start:
			raise CFException("Impossible to split a basic block at its starting address")

		newBB = BasicBlock(splitAdd, previousBB.end)
		previousBB.end = splitAdd - 4
		newBB.callTarget = previousBB.callTarget
		previousBB.callTarget = None

		indexNewBB = len(self.BBs)

		self.BBs.append(newBB)

		self.startMap[newBB.start] = indexNewBB
		self.endMap[newBB.end] = indexNewBB
		self.endMap[previousBB.end] = splitNum

		return indexNewBB

	def __iter__(self):
		return self.BBs.__iter__()

class CFG(GraphClass):
	logger = loggingConf.getLogger("CFG")

	def __init__(self, function):
		super().__init__("DiGraph")

		self._function = function

		self.bbs = BasicBlockList()

		self.graph.add_node('start')
		self.graph.add_node('end')

	def linkToStart(self, add):
		n = self.bbs.getBBNumFromStart(add)
		self.graph.add_edge('start', n)

	def linkToEnd(self, add):
		n = self.bbs.getBBNumFromEnd(add)
		self.graph.add_edge(n, 'end')

	def addBB(self, bb, **att):
		self.bbs.addBB(bb)
		bb._function = self._function

		n = self.bbs.getBBNumFromStart(bb.start)
		self.graph.add_node(n, **att)

	def addEdge(self, end, target, **att):
		try:
			e1 = self.bbs.getBBNumFromEnd(end)
		except CFException:
			self.logger.debug("""e1 not found, running fallback""")
			# The adress is not the ending point of a BB, we search for
			# the basic block containing the address
			index = self.bbs.getBBNum(end)

			if index == -1:
				raise CFException("An error occured: unable to find a basic block with the end address")

			self.splitBB(index, end, False)

			# We update e1
			e1 = self.bbs.getBBNumFromEnd(end)

		try:
			e2 = self.bbs.getBBNumFromStart(target)

		except CFException:
			self.logger.debug("""e2 not found, runnning fallback""")
			# The adress is not the starting point of a BB, we search for
			# the basic block containing the address
			index = self.bbs.getBBNum(target)

			if index == -1:
				raise CFException("An error occured: unable to find a basic block with the target address")

			self.splitBB(index, target, True)

			# We update e2
			e2 = self.bbs.getBBNumFromStart(target)

		self.graph.add_edge(e1, e2, **att)

	def splitBB(self, BBNum, splitAdd, splitAddToNewBB):
		# Split the basic block at the split address and create a new BB beginning at the split address
		self.logger.debug(""" Splitting a BasicBlock at the address : %s """ % hex(splitAdd))

		newBBNum = self.bbs.split(BBNum, splitAdd, splitAddToNewBB)

		successorBB = set(self.graph.successors(BBNum))

		# Update the CFG
		for i in successorBB:
			self.graph.remove_edge(BBNum, i)
			self.graph.add_edge(newBBNum, i)

		self.graph.add_edge(BBNum, newBBNum)

	def __str__(self):
		return self.__repr__()

	def __repr__(self):
		return '<CFG \''+self._function.getName()+'\'>'


class ICFG(GraphClass):
	"""
	Links between the functions of a program
	"""

	def __init__(self, program):
		super().__init__("MultiDiGraph")
		self._program = program

	def addFunction(self, f):
		self.graph.add_node(f)

	def addCall(self, caller, callee, bb):
		self.graph.add_edge(caller, callee, callBB=bb)
