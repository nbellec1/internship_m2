# -*- coding: utf-8 -*-

from utils import loggingConf

class Instruction(object):
	"""
	An interface for every instructions
	"""

	instructionTypes = ['unknown', 'branch', 'call', 'jump', 'ret', 'other']

	def __init__(self, t = 'unknown', code = None, address = None, opcode=None, target=None):
		if t not in self.instructionTypes:
			raise Exception("Instruction type not correct : %s" % str(t))
		self._type = t
		self._code = code
		self._target = target
		self._address = address
		self._opcode = opcode

	def getType(self): return self._type
	def getCode(self): return self._code
	def getTarget(self): return self._target
	def getAddress(self): return self._address
	def getOpcode(self): return self._opcode

class BasicBlock(object):
	"""
	Define the basic blocks of a program
	"""

	def __init__(self, start=None, end=None, function=None):
		self.start = start
		self.end = end
		self.callTarget = None
		self._function = function

	def setCallTarget(self, add):
		self.callTarget = add

	def addInst(self):
		self.end += 4

	def __contains__(self, add):
		if add % 4 != 0:
			raise Exception(" The instruction address is not a multiple of 4 ", self)
		return add >= self.start and add <= self.end

	def __str__(self):
		return self.__repr__()

	def __repr__(self):
		return '<BB start :' + hex(self.start) + ', end :'+hex(self.end)+'>'

class Function(object):
	"""
	An interface regrouping every informations we have on a function
	"""

	def __init__(self, name, start, program):
		# Amoung the informations : CFG, SESE regions, recursive, safe, name
		self.name = name
		self.safe = True
		self.recursive = False
		self.start = start # Could be just the first  instruction stocked
		self.instructions = list()
		self.cfg = None
		self.pdst = None
		self._program = program

	""" Getters """

	def getName(self):
		return self.name

	def isSafe(self):
		return self.safe

	def isRecursive(self):
		return self.recursive

	def getAddress(self):
		return self.start

	def getInstructions(self):
		return self.instructions

	def getCFG(self):
		return self.cfg

	def getPdst(self):
		return self.pdst

	def getStart(self):
		return self.start

	""" End Getters """

	def addInstruction(self, inst):
		self.instructions.append(inst)

	def __str__(self):
		return self.__repr__()

	def __repr__(self):
		return '<Function \''+self.name+'\'>'

class Program(object):
	"""
	Represent the information of a whole program
	"""

	logger = loggingConf.getLogger("Program")

	def __init__(self, name, binaryPath):
		self.name = Program._sanitize(name)
		self.binaryPath = binaryPath
		self.functions = list()
		self.funcAddMap = dict()
		self.funcNameMap = dict()
		self.icfg = None
		self.labelIndependantHeader = None

	@staticmethod
	def _sanitize(name):
		return name.split('.')[0].split(' ')[0]

	def __iter__(self):
		return self.functions.__iter__()

	def getName(self):
		return self.name

	def addFunction(self, f):
		index = len(self.functions)
		self.functions.append(f)
		self.funcAddMap[f.getStart()] = index
		self.funcNameMap[f.getName()] = index
		f._program = self

	def getFunctionFromName(self, name):
		funcIndex = self.funcNameMap.get(name)
		if funcIndex == None:
			raise Exception("The function \"%s\" is not present in the program \"%s\"" % (name, self.name))

		return self.functions[funcIndex]
