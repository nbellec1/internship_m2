# -*- coding: utf-8 -*-

def isSecured(f):
    return f.isSafe() and not f.isRecursive()