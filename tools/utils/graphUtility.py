# -*- coding: utf-8 -*-

import graphviz as gvz
import networkx as nx

from enum import Enum

def printGraph(G, filename="graph", cleanup=True):
	dot = gvz.Digraph()
	dot.format = 'pdf'

	for node in G.nodes:
		dot.node(str(node), str(node))

	if G.is_multigraph():
		for s,t,_ in G.edges:
			dot.edge(str(s),str(t))
	else:
		for s,t in G.edges:
			dot.edge(str(s),str(t))

	dot.view(filename, cleanup=cleanup)

def printDotPDST(cfg, pdst, filename="pdst", cleanup=True):
	colorList = ['blueviolet', 'brown','cadetblue','chartreuse','chocolate','aquamarine', 'cornflowerblue','crimson','cyan','darkgoldenrod1','darkorange','deeppink','darkturquoise','forestgreen']
	index = 0

	pdstGraph = pdst.getGraph()

	def recursion(currentSubgraph, currentSese):
		nonlocal index
		nonlocal colorList
		nonlocal pdstGraph

		ColorIndex = index

		restricted_nodes = currentSese.nodes

		for n in restricted_nodes:
			currentSubgraph.node(str(n))

		for sese in pdstGraph.neighbors(currentSese):
			if sese.isDomain():
				for region in pdstGraph.neighbors(sese):
					with currentSubgraph.subgraph(name='cluster'+str(region.num), graph_attr={'shape':'box', 'color':colorList[ColorIndex%len(colorList)]}) as c:
						index += 1
						recursion(c, region)
			else:
				with currentSubgraph.subgraph(name='cluster'+str(sese.num), graph_attr={'shape':'box', 'color':colorList[ColorIndex%len(colorList)]}) as c:
					index += 1
					recursion(c, sese)

	dot = gvz.Digraph(name='exemple')

	startingSese = pdst.getRoot()
	recursion(dot, startingSese)

	for s,t in cfg.edges:
		dot.edge(str(s),str(t))

	dot.view(filename, cleanup=cleanup)

class GraphClass(object):
	"""
	An abstract base class that allows to factorize the basic utility of graph-based class
	"""

	def __init__(self, graphType):
		self.graph = nx.__getattribute__(graphType)()

	def getGraph(self):
		return self.graph

	def getFullChildren(self, node, predicat = lambda nn : True):
		children = []

		def dfs(n):
			nonlocal children

			for child in self.graph.successors(n):
				if child == node or child in children or not predicat(node):
					continue
				children.append(child)
				dfs(child)

		dfs(node)
		return children

	def print(self, filename="graph.pdf", cleanup=True):
		printGraph(self.graph, filename, cleanup)

	def __iter__(self):
		return self.graph.nodes().__iter__()

	def __contains__(self, n):
		return n in self.graph.nodes
