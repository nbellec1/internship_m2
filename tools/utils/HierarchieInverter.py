# -*- coding: utf-8 -*-

class HierarchyInverter(object):
	def __init__(self, project):
		__slots__ = ['sese_to_func',  'BB_to_func', 'func_to_prog', 'project']
		self.sese_to_func = dict()
		self.BB_to_func = dict()
		self.func_to_prog = dict()
		self.project = project

		self.initialize()

	def initialize(self):
		None

	def hierarchyFromSese(self, sese):
		for P in self.project.programs:
			for function in P.functions:
				pdst = function.pdst
				if sese in pdst.getGraph().nodes():
					return P, function

	def hierarchyFromFunction(self, func):
		for P in self.project.programs:
			if func in P.functions:
				return P

	def hierarchyFromBB(self, bb):
		for P in self.project.programs:
			for function in P.functions:
				cfg = function.cfg
				if bb in cfg.bbs.BBs:
					return P, function
