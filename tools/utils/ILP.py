# -*- coding: utf-8 -*-

import cvxpy as cv
import numpy as np
from functools import wraps
from itertools import combinations
from collections import defaultdict

# An example of IPL

def switchLog(prefix=""):
	def decorator(fun):
		@wraps(fun)
		def wrapper(self):
			oldLogLevel = self.logLevel
			self.logLevel = prefix + fun.__name__
			self.print("Start \"%s\""%(prefix + fun.__name__))
			fun(self)
			self.print("End \"%s\""%(prefix + fun.__name__))
			self.logLevel = oldLogLevel
		return wrapper
	return decorator

class SESERegion:
	def __init__(self, j1, j2):
		self.j1 = min(j1, j2)
		self.j2 = max(j1, j2)
		
	def __contains__(self, j):
		return self.j1 <= j and j <= self.j2

class SeseSelectionProblem:
	"""
	Basic Sese selection problem (with unique cache cost, no extension)
	"""
	def __init__(self, C, R, T, n, I, J, W, F, fairFactor):
		
		self.logs = defaultdict(str)
		self.logLevel = "init"
		
		# A mapping between the indexes and the SESE regions
		self.SeseMapping = []
		# The maximum cache size
		self.C = C
		# The memory each region would take if selected
		self.R = R
		# The security WCET threshold
		self.T = T
		# The number of programs
		self.n = n
		# The number of SESE domains for each program
		self.I = I.copy() 
		# The number of canonical SESE regions for each domain of each program
		self.J = J.copy()
		# The WCET of each SESE region (canonical or not)
		self.W = W.copy()
		# The coverage of each SESE region (canonical or not)
		self.F = F.copy()
		# The fairness factor
		self.FairFactor = fairFactor
		
		# The boolean variables representing the selection of SESE regions
		self.selection = [[cv.Variable(self.J[p][i]*(self.J[p][i] + 1)//2, boolean=True, integer=True) for i in range(self.I[p])] for p in range(self.n)]
		
		# The different constraints of our problem:
		#   * WCET constraints (could be eliminated) ( ~(nb of regions)**2 constraints )
		#   * No memory overload constraints         ( 1 constraint but can be tricky if cache optimisation enabled )
		#   * Non overlapping constraints            ( ~(nb of regions)**2 constraints )
		#   * Fairness constraints                   ( (p-1)*p/2 constraints )
		self.WcetConstraints = []
		self.cacheConstraint = []
		self.nonOverlappingConstraints = []
		self.fairnessConstraints = []
		self.coverageFormula = []
		
		# The goal function to maximize
		self.goal = None
		self.P = None
		self.r = None
		
		self.generateSESERegions()
		self.generateWCETConstraints()
		self.generateNonOverlappingConstraints()
		self.generateFairnessConstraints()
		self.generateCacheConstraint()
		self.generateGoal()
		
	def print(self, s, end='\n'):
		#print(s,end=end)
		self.logs[self.logLevel] += s + end
	
	@switchLog()
	def generateSESERegions(self):
		# Generate the SESE mapping
		
		for p in range(self.n):
			self.SeseMapping.append([])
			for i in range(self.I[p]):				
				self.SeseMapping[p].append([])
				j_max = self.J[p][i]
				
				self.print("Generate canonical SESE regions from program %d : (%d,0) -> (%d, %d)"%(p,i,i,j_max))
				
				for j in range(j_max):
					self.SeseMapping[p][i].append(SESERegion(j,j))
					self.print("\t%d"%(j))
				
				self.print("Generate non canonical SESE regions from program %d : (%d,0) -> (%d, %d)"%(p,i,i,j_max))
				
				for j1,j2 in combinations(range(j_max), r=2):
					self.SeseMapping[p][i].append(SESERegion(j1,j2))
					self.print("\t%d -> %d"%(j1, j2))
	
	@switchLog()			
	def generateWCETConstraints(self):
		for p in range(self.n):
			for i in range(self.I[p]):
				self.print("\tNew constraint : W[%d][%d] * var[%d][%d] <= [T]*len(W[%d][%d])" % (p,i,p,i,p,i))
				self.WcetConstraints.append(
					cv.multiply(self.W[p][i], self.selection[p][i]) <= np.array([self.T]*len(self.W[p][i]))
				)
	
	@switchLog()
	def generateNonOverlappingConstraints(self):
		for p in range(self.n):
			for i in range(self.I[p]):
				self.print("New constraint for p = %d, i = %d : " % (p,i))
				for base in range(self.J[p][i]):
					associatedVar = []
					first = True
					self.print("\t",end="")
					for numRegion in range(len(self.SeseMapping[p][i])):
						if base in self.SeseMapping[p][i][numRegion]:
							associatedVar.append(self.selection[p][i][numRegion])
							
							if not first:
								self.print(" + ",end="")
							self.print(str(numRegion), end="")
							first = False
					
					self.print(" <= 1")
					
					self.nonOverlappingConstraints.append(cv.sum(associatedVar) <= 1)
	
	@switchLog()
	def generateCoverageFormula(self):
		if len(self.coverageFormula) != 0:
			return
		
		for p in range(self.n):
			self.print("Generate coverage formula for p = %d" % (p))
			coverageAtoms = []
			
			first = True
			for i in range(self.I[p]):
				self.print("\t",end="")
				if not first:
					self.print("+",end="")
				else:
					self.print(" ", end="")
				self.print(" var[%d][%d] * F[%d][%d]" % (p,i,p,i))
				first = False
				coverageAtoms.append(self.selection[p][i] * self.F[p][i])
			
			self.coverageFormula.append(cv.sum(coverageAtoms))
	
	@switchLog()
	def generateFairnessConstraints(self):
		if len(self.coverageFormula) == 0:
			self.generateCoverageFormula()
		
		for p1, p2 in combinations(range(self.n), r=2):
			self.print("Generate fairness constraint for (p1,p2) = (%d,%d)"%(p1,p2))
			self.fairnessConstraints.append(cv.abs(self.coverageFormula[p1] - self.coverageFormula[p2]) <= self.F)
	
	@switchLog()
	def generateCacheConstraint(self):
		cacheSum = 0
		
		first = True
		for p in range(self.n):
			for i in range(self.I[p]):
				self.print("\t",end="")
				if not first:
					self.print("+",end="")
				else:
					self.print(" ",end="")
				self.print(" sum(var[%d][%d])"%(p,i))
				first = False
				
				cacheSum += cv.sum(self.selection[p][i])
		
		self.print("  <= C")
		
		self.cacheConstraint.append(self.R * cacheSum <= self.C)
	
	@switchLog()
	def generateGoal(self):		
		if len(self.coverageFormula) == 0:
			self.generateCoverageFormula()
		
		self.goal = cv.sum(self.coverageFormula)
	
	@switchLog()
	def solve(self):
		self.P = cv.Problem(cv.Maximize(self.goal), self.WcetConstraints+self.cacheConstraint+self.nonOverlappingConstraints+self.fairnessConstraints)
		self.r = self.P.solve(solver=cv.GLPK_MI)
		
		self.print("Result : %f"%(self.r))
		self.print("status : %s"%(self.P.status))
		
		if self.P.status != cv.OPTIMAL:
			print("[WARNING] Solution found may be less than optimal")

class SeseSelectionProblemNonConstantCost(SeseSelectionProblem):
	def __init__(self, C, R, T, n, I, J, W, F, fairFactor):
		if type(R) != list:
			raise Exception("Error, R is not a list")
			
		super().__init__(C, R.copy(), T, n, I, J, W, F, fairFactor)
		
	@switchLog(prefix="nonConstantCost::")
	def generateCacheConstraint(self):		
		cacheSum = 0
		
		first = True
		for p in range(self.n):
			for i in range(self.I[p]):
				self.print("\t",end="")
				if not first:
					self.print("+",end="")
				else:
					self.print(" ",end="")
				self.print(" sum(R[%d][%d] * var[%d][%d])"%(p,i,p,i))
				first = False
				
				cacheSum += cv.sum(cv.multiply(self.R[p][i], self.selection[p][i]))
		
		self.print("  <= C")
		
		self.cacheConstraint.append(cacheSum <= self.C)

class SeseSelectionProblemMemoryNonConstantWithCompression(SeseSelectionProblem):
	def __init__(self, C, R, T, n, I, J, W, F, fairFactor, compressionGain):
		if type(R) != list:
			raise Exception("Error, R is not a list")
		
		self.compressionGain = compressionGain.copy()
		super().__init__(C, R.copy(), T, n, I, J, W, F, fairFactor)
		
	
	def recoverSelectionFromPredicat(self, p, predicat):
		i,j1,j2 = predicat
		assert( j1 < self.J[p][i] and j2 < self.J[p][i] )
		place = None
		if j1 == j2:
			place = j1
			
		else:
			j_min = min(j1,j2)
			j_max = max(j1,j2)
			
			place = self.J[p][i] - 1
			place += (self.J[p][i] - 1) * j_min - ((j_min -1)*j_min//2)
			place += j_max - j_min
			
		return (i, place)
	
	@switchLog(prefix="MemoryNonConstantWithCompression::")	
	def generateCacheConstraint(self):
		cacheSum = 0
		
		first = True
		for p in range(self.n):
			for i in range(self.I[p]):
				self.print("\t",end="")
				if not first:
					self.print("+",end="")
				else:
					self.print(" ",end="")
				self.print(" sum(R[%d][%d] * var[%d][%d])"%(p,i,p,i))
				first = False
				
				cacheSum += cv.sum(cv.multiply(self.R[p][i], self.selection[p][i]))
		
			for predicats in self.compressionGain[p]:
				self.print("\t- ",end="")
				predicatVar = []
				predicatIJ = []
				first = True
				for predicat in predicats:		
					i, j = self.recoverSelectionFromPredicat(p, predicat)
					self.print(" var[%d][%d][%d] *" % (p,i,j),end="")
					
					predicatVar.append(self.selection[p][i][j])
					predicatIJ.append((i,j))
				
				self.print(" Gain[%d][\"%s\"]"%(p, str(predicatIJ)))
				cacheSum -= cv.min(cv.vstack(predicatVar))*self.compressionGain[p][predicats]
		
		self.print("  <= C")
		
		self.cacheConstraint.append(cacheSum <= self.C)
		
def _generateWDebug(baseW):
	n = len(baseW)
	W = baseW.copy()
	
	for j1,j2 in combinations(range(n), r=2):
		w = 0
		for j in range(j1, j2+1):
			w += baseW[j]
		W.append(w)
	
	return W

problem = SeseSelectionProblemMemoryNonConstantWithCompression(5, [[[1,3,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]], 3, 1, [2], [[3,5]], [[[1,3,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]], [[[1,2,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]], 10, [{frozenset([(0,1,1), (1,0,0)]):2}])

#problem = SeseSelectionProblemNonConstantCost(5, [[[1,3,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]], 3, 1, [2], [[3,5]], [[[1,3,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]], [[[1,2,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]], 10)
#problem = SeseSelectionProblem(5, 1, 3, 1, [2], [[3,5]], [[[1,3,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]], [[[1,2,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]], 10)
problem.solve()

#SESE_mapping = [[[],[]]]

#SESE_mapping[0][1] = generateSESERegions(5)
#SESE_mapping[0][0] = generateSESERegions(3)

#C = 5
#R = [[np.array([1,3,1,4,5,4]), [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]]
#T = 3
#n = 1
#I = [2]
#J = [[3,5]]

#baseW = [[[1,3,1], [1,1,1,1,1]]]
#baseF = [[[1,3,1], [1,1,1,1,1]]]

##W = [[_generateWDebug(baseW[p][i]) for i in range(I[p])] for p in range(n)]
##F = [[_generateWDebug(baseF[p][i]) for i in range(I[p])] for p in range(n)]

#W = [[[1,3,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]]
#F = [[[1,2,1,4,5,4], [1,1,1,1,1,2,3,4,5,2,3,4,2,3,2]]]

#seseRegionsVar = [[cv.Variable(J[p][i]*(J[p][i] + 1)//2, boolean=True) for i in range(I[p])] for p in range(n)]

#WCET_constraint = generateWCETConstraints(W,seseRegionsVar, T, n, I)

#cacheGain = 0 - 2 * cv.min(cv.vstack([seseRegionsVar[0][0][1], seseRegionsVar[0][1][0]]))

#Cache_constraint = cv.sum(cv.multiply(R[0][0], seseRegionsVar[0][0])) + cv.sum(cv.multiply(R[0][1], seseRegionsVar[0][1])) <= C#+ cacheGain <= C

##L = [0,1,0,0,0,0,1]+[0]*(15-7)

#CacheGain = 0 #- 1 * cv.min(cv.multiply(seseRegionsVar[0][0], L))

##WCET_constraint = cv.multiply(W[0][0], seseRegionsVar[0][0]) <= np.array([T]*len(W[0][0]))
##Cache_constraint = R * cv.sum(seseRegionsVar[0][0]) + CacheGain <= C

### Will require a specific structure to handle the conversion between variables and SESE regions

##Non_overlapping_constraint = generateNonOverlappingConstraints(SESE_mapping[0][0], seseRegionsVar[0][0], J[0][0])

#F = [F[0][0] * seseRegionsVar[0][0], F[0][1] * seseRegionsVar[0][1]]

#G = cv.sum(F)

#P = cv.Problem(cv.Maximize(G), [Cache_constraint])#+Non_overlapping_constraint)
#r = P.solve()
