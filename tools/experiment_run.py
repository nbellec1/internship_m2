# -*- coding: utf-8 -*-

from pathlib import Path
import subprocess as sp
import time

benchmarkInf10 = [
 'bs',
 'bsort100',
 'cnt',
 'crc',
 'expint',
 'fdct',
 'fibcall',
 'fir',
 'insertsort',
 'jfdctint',
 'lcdnum',
 'minmax',
 'ns',
 'prime',
 'qurt',
 'select',
 'sqrt'
]

benchmarkInf15 = [
 'bs',
 'bsort100',
 'cnt',
 'crc',
 'expint',
 'fdct',
 'fft',
 'fibcall',
 'fir',
 'insertsort',
 'jfdctint',
 'lcdnum',
 'ludcmp',
 'matmult',
 'minmax',
 'minver',
 'ns',
 'prime',
 'qurt',
 'select',
 'simple',
 'sqrt',
 'ud'
]

benchmark = [
	"adpcm",
	"bs",
	"bsort100",
	"cnt",
	"compress",
	"crc",
	"expint",
	"fdct",
	"fft",
	"fibcall",
	"fir",
	"insertsort",
	"jfdctint",
	"lcdnum",
	"ludcmp",
	"matmult",
	"minmax",
	"minver",
	"ndes",
	"ns",
	"nsichneu",
	"prime",
	"qurt",
	"select",
	"simple",
	"sqrt",
	"statemate",
	"ud"
]

# experimentDir = Path('experiment_full/').absolute()
# objdumpPath = Path('/opt/bcc-2.0.5-gcc/bin/sparc-gaisler-elf-objdump')
# buildPath = Path("../build/").absolute()
# AISPath = Path("../benchmarks/AIS/").absolute()

# benchTimePath = experimentDir.joinpath("benchTime.py").absolute()

# benchTime = {}

#experimentDir.mkdir()

# Generate experiment with unlimited memory
# for bench in benchmarkInf15:
	# analysisPath = experimentDir.joinpath('%s_full' % bench).absolute()
	# exePath = buildPath.joinpath(bench+'.exe')
	# ais = AISPath.joinpath(bench+'.ais')
	# CSVResultPath = analysisPath.joinpath('localWcet.csv').absolute()
	# resultPath = experimentDir.joinpath('localWcet_%s_full.csv' % bench).absolute()
	# start = time.time()
	# p = sp.run(['python3','Controller.py',str(exePath),str(objdumpPath),str(ais),"-d",str(analysisPath)], stdout=sp.PIPE)
	# end = time.time()
	# benchTime[bench + "_full"] = end - start
	# sp.run('cp %s %s' % (str(CSVResultPath), str(resultPath)), shell=True)
	
# with benchTimePath.open('w') as f:
	# f.write("benchTime = "+str(benchTime))

fullExpDir = Path('experiment_full/').absolute()
partialExpDir = Path('experiment_partial/').absolute()
resultDir = Path("experiment_csv_analysis/").absolute()

for bench in benchmarkInf10:
	csvPartial = partialExpDir.joinpath('%s_partial/localWcet.csv' % bench).absolute()
	csvFull = fullExpDir.joinpath('%s_full/localWcet.csv' % bench).absolute()
	
	csvResultPartial = Path("localWcet_%s_partial.csv" % bench)
	csvResultFull = Path("localWcet_%s_full.csv" % bench)
	
	sp.run("cp %s %s" % (str(csvPartial), str(csvResultPartial)), shell=True)
	sp.run("cp %s %s" % (str(csvFull), str(csvResultFull)), shell=True)
	
	sp.run("Rscript rplot.R %s" % bench, shell=True)
	sp.run("mv Rplots.pdf %s" % str(resultDir.joinpath("%s.pdf" % bench)), shell=True)
	sp.run("rm %s %s" % (str(csvResultPartial), str(csvResultFull)), shell=True)

