int main(int argc, const char* argv[]) {
	unsigned int sum = 0;
	
	for (unsigned int i = 0; i < 30; i++) {
		sum += i;
	}
	
	return sum;
}
