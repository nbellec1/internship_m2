unsigned char odd(unsigned int i);
unsigned char even(unsigned int i); 

unsigned char odd(unsigned int i) {
	unsigned char r = 0; // False
	
	if (i != 0)
		r = even(i-1);
		
	return r;
}

unsigned char even(unsigned int i) {
	unsigned char r = 1; // True
	
	if (i != 0)
		r = odd(i-1);
		
	return r;
}



int main(int argc, const char* argv[]) {
	unsigned int i = 20; 
	
	return even(i);
}
