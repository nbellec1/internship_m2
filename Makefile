IDIR=include
CC=sparc-gaisler-elf-gcc
CFLAGS=-I$(IDIR) -O0 -mcpu=leon3
BUILD=build

ODIR=obj
LDIR=lib

SDIR=benchmarks

.PHONY : all-example clean 

example-all: example-heptane example-perso

example-heptane: adpcm.exe bs.exe bsort100.exe cnt.exe compress.exe crc.exe expint.exe fdct.exe fft.exe fibcall.exe fir.exe insertsort.exe jfdctint.exe lcdnum.exe ludcmp.exe matmult.exe minmax.exe minver.exe ndes.exe ns.exe nsichneu.exe prime.exe qurt.exe select.exe simple.exe sqrt.exe statemate.exe ud.exe

example-perso: simplest.exe loop1.exe loop2.exe function1.exe function2.exe doubleFunction.exe loop3.exe

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS) -I$(SDIR)

%.exe: $(ODIR)/%.o
	$(CC) -o $(BUILD)/$@ $^ $(CFLAGS)

clean:
	@echo "Cleaning"
	rm -f $(ODIR)/*.o $(BUILD)/*
